$(document).ready(function() {
    $('#menu-btn').on('click', function (e) {
        $('#menu').toggle();
    });

    $(document).mouseup(function (e) {
        let blockMenu = $("#menu");
        let blockMenuBtn = $("#menu-btn");
        if (!blockMenuBtn.is(e.target) && blockMenuBtn.has(e.target).length === 0
            && !blockMenu.is(e.target) && blockMenu.has(e.target).length === 0) {
            blockMenu.hide();
        }
    });
   
});