var gulp           = require('gulp'),
    watch          = require('gulp-watch'),
    sass           = require('gulp-sass'),
    livereload     = require('gulp-livereload'),
    connect        = require('gulp-connect');


var plumberErrors = function(err) {
  notify.onError({
    title    : 'Gulp',
    subtitle : 'Failure!',
    message  : 'Error: <%= error.message %>',
    sound    : 'Bee'
  })(err);

  this.emit('end');
};

gulp.task('livereload', function (){
  gulp.src('public/**/*')
  .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch('sources/scss/*.scss', {interval : 300}, gulp.series('sass'));
  livereload.listen();
  gulp.watch(['sources/scss/*.scss', 'sources/css/*.css', './*.html']).on('change', livereload.changed);
});

gulp.task('sass', function () {
  return gulp.src('sources/scss/*.scss')
      .pipe(sass({ errLogToConsole: true }))
      .pipe(gulp.dest('public/css'));
});
gulp.task('default', gulp.parallel(['livereload', 'watch', 'sass']));

